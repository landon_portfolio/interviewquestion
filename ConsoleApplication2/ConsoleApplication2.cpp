// ConsoleApplication2.cpp : Defines the entry point for the console application.
//c++ parser to collect and organize JSON token heirarchy
//attempted by Landon Taylor starting at 3-18-2016, 4:00pm

//collect tokens at 11:11am , 3-19-2016; getting stuck. 
	//using regex_search. 
//need to remove dups
//no order(iterates through array of regexes to check): fix later

//finds first string. 
//finds : correctly
//finds { alone, or after colon correctly
	//regex setup for  [ and ]  broken (mismatch error, tried to mimic {} regex )
	//need to find any secondary quoted strings after : (a single string, array of strings)


//8:36pm
//created bitbucket repository
//changed regex and catches more, not catching everything.

//planning 


//3-20-2016 read jsmn implementation
//using enum types to parse JSON through myClass
//store different types of pointers in dedicated vector type.
//cleaning up old methods/comments, stored history on bitbucket.
//4:14pm. attempted to tokenize primitive values

//8:02
//parsing for tokens seems complete
//now i need to generate correct token pairs


#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <exception>
#include <vector>
#include <stdexcept>
#include <windows.data.json.h>

using namespace std;

//http://zserge.com/jsmn.html

typedef enum {
	jprimitive = 0,
	jobject = 1,
	jarray = 2,
	jstring = 3,
	jtoken = 4,
} JSONtypes;
typedef struct {
	JSONtypes type;
	int start;
	int end;
	int size;
} JSONtokens;


// http://www.cplusplus.com/doc/tutorial/exceptions/
class myexception : public exception
{
	virtual const char* what() const throw()
	{
		return "My exception happened, cant convert size_t to int";
	}
} myex;


unsigned int convert(size_t what)
{
	if (what > UINT_MAX) {
		throw myex;
	}
	return static_cast<unsigned int>(what);
}

//This object has methods to store pointers of the JSON types' start and ending indexes.
//I assume the format/syntax of the JSON tokens pairs are not broken. (for now)
//parsing for tokens seems complete
//now i need to generate correct token pairs
class myJSONParser {
	//http://zserge.com/jsmn.html
public:
	string JSONtext;
	vector<JSONtokens> myTokenPairs; //key : value
	vector<JSONtokens> myObjects;	//{ }
	vector<JSONtokens> myArrays;	// [] 
	vector<JSONtokens> myStrings;	// "  " ,  : 
	vector<JSONtokens> myPrimitives;	//#, null, bool

	myJSONParser() {  }
	myJSONParser(string s)
	{
		JSONtext = s;
		parseObjects(s);
		parseArrays(s);
		parseStrings(s);
		parsePrimitives(s);

		//createtokenpairs; // start(string) : end(value)
		parseTokenPairs(s);
		printMyTokenPairs();
	}

	void printMyArrays() {
		cout << "[" << endl;
		for (int i = 0; i < myArrays.size(); i++) {
			int start = myArrays[i].start; 
			int end = myArrays[i].end;
			cout << myArrays[i].type << " , " <<myArrays[i].start << " , " <<myArrays[i].end << " , " <<myArrays[i].size;
			cout << " = ";
			for (int j = start; j <= end; j++)
				cout << JSONtext[j] ;
			cout << endl;
		}
		cout << "]" << endl;
		//print the text version
		cout << endl;
	}
	
	void parseArrays(string s) { 
		JSONtokens arrayItem;
		int counter = 0;
		int index;
		size_t found = s.find("[");
		if (found != std::string::npos)
		{
			index = convert(found);										//type_t to int
			found++;													//iterate so that we can move past this token
			//cout << "found outer object at:" << index << endl;
			myArrays.push_back(arrayItem = { jarray, index } );		//push onto 0th index 
			while ((found = s.find("[", found)) != std::string::npos)	//find inner {'s
			{
				counter++;
				index = convert(found);
				found++;
				myArrays.push_back(arrayItem = { jarray, index });
			}
			found = index;

			while ((found = s.find("]", found)) != std::string::npos)
			{
				index = convert(found);
				found++;
				myArrays[counter].end = index;
				myArrays[counter].size = myArrays[counter].end - myArrays[counter].start;
				counter--;		//counter will be -1 at the end
			}
		}
		cout << "end parseArrays function" << endl;
		return;
	}
	void printMyStrings() {
		cout << "\" " << endl;
		for (int i = 0; i < myStrings.size(); i++) {
			int start = myStrings[i].start;
			int end = myStrings[i].end;
			cout << myStrings[i].type << " , " <<myStrings[i].start << " , " <<	myStrings[i].end << " , " << myStrings[i].size;
			cout << " = " << endl;

			for (int j = start; j <= end; j++)
				cout << JSONtext[j];
			cout << endl;
		}
		cout << "\" " << endl;
		
	}
	void parseStrings(string s) {
		JSONtokens stringItem;
		JSONtokens tokenItem;
		int counter = 0;
		int index;
		size_t found=0;	//find first "
		while ( (found= s.find("\"", found)) != std::string::npos)	//if found first matching quote
		{
			index = convert(found);										//type_t to int
			found++;													//iterate so that we can move past this token
			//cout << "found an opening quote at:" << index << endl;
			myStrings.push_back( stringItem = { jstring, index });		//create object at correct index
			//counter++;
			//create add closing quote
			if((found = s.find("\"", found)) != std::string::npos)
			{
				//find the closing quote
				//assume things are good, found the closing quote
				index = convert(found);
				found++;
				//cout << "found closing quote at : " << index << endl;
				myStrings[counter].end = index;
				myStrings[counter].size = myStrings[counter].end - myStrings[counter].start;
				counter++;
			}

			//found closing quote, search for trailing :
			/*
			if (s[index++] == ':')
			{
				tokenItem.type = jstring;
				tokenItem.start = myStrings[counter--].start;

			}
			*/
			//found = index;
		}

		//find and make tokens for : character

		found = 0;
		while ((found = s.find(":", found)) != std::string::npos) {
			index = convert(found);										//type_t to int
			found++;													//iterate so that we can move past this token
			myStrings.push_back(stringItem = { jstring, index, index, 1 });		//create object at correct index
		}

		cout << "end parseStrings function" << endl;
		return;

	}

	void printMyPrimitives() {
		cout << "( primitives:" << endl;
		for (int i = 0; i < myPrimitives.size(); i++) {
			int start = myPrimitives[i].start;
			int end = myPrimitives[i].end;
			cout << myPrimitives[i].type << " , " << myPrimitives[i].start << " , " << myPrimitives[i].end << " , " << myPrimitives[i].size;
			cout << " = ";
			for (int j = start; j <= end; j++)
				cout << JSONtext[j];
			cout << endl;
		}
		cout << ") " << endl;
	}
	void parsePrimitives(string s) {
		JSONtokens primitiveItem;
		int counter = 0;
		int index;
		size_t found = 0;	//find digits, nulls , bools
		//primitives always follow a :
		while ((found = s.find(":", found)) != std::string::npos)
		{
			
			index = convert(found);
			found++;
			index++;
			//myStrings.push_back(stringItem = { jstring, index });	
			//ignore whitespace until value found
			while (s[index] == ' ')
			{
				index = convert(found);
				found++;
				index++;
			}

			string sIndex;
			sIndex = s[index];
			//cout << sIndex <<endl;

			//this doesn't catch what i need
			if (s[index] == 0x22  || s[index]== ("\\" || "\"" || "{" || "[")) {
				index++; 
				found++;
				continue;
			}

			///maybe incomplete
			if (s[index] == ('t' || 'f'))
			{
				index = convert(found);
				myPrimitives.push_back(primitiveItem = { jprimitive, index });
				//move index to the end of token, record end and size
				index++;
				if (s[index] == 'r') {

					myPrimitives[counter].size = 4; myPrimitives[counter].end = myPrimitives[counter].start + 4;
				}
				else
					myPrimitives[counter].size = 5; myPrimitives[counter].end = myPrimitives[counter].start + 5;

				counter++;
			}
			///maybe incomplete
			if (s[index] == '-' || s[index] == '1' ||
				s[index] == '2' || s[index] == '3' ||
				s[index] == '4' || s[index] == '5' ||
				s[index] == '6' || s[index] == '7' ||
				s[index] == '8' || s[index] == '9') 
			{
				index = convert(found);
				myPrimitives.push_back(primitiveItem = { jprimitive, index });
				index++;
				found++;
				//check for trailing digits
				while (s[index] == ('1' || '2' || '3' || '4' || '5' || '6' || '7' || '8' || '9'))
				{
					index++;
					found++;
				}
				//no more digits, index did not increment
				myPrimitives[counter].end = index;
				myPrimitives[counter].size = myPrimitives[counter].end - myPrimitives[counter].start;
				//move index to end of stringDigits token, record end and size
				counter++;
			}
			
			if (s[index] == 'n')
			{
				//always null, 
				myPrimitives.push_back(primitiveItem = { jprimitive, index, index+3, 4});
			}

			
		}//end while

	}

	void printMyObjects() {
		cout << "{" << endl;
		for (int i = 0; i < myObjects.size(); i++)
		{
			int start = myObjects[i].start;
			int end = myObjects[i].end;
			cout << myObjects[i].type << " , " <<myObjects[i].start << " , " <<	myObjects[i].end << " , " <<myObjects[i].size;
			cout << " = ";

			for (int j = start; j <= end; j++) {
				cout << JSONtext[j];
			}
			cout << endl;
		}
		cout << "}" << endl;
	}

	void parseObjects(string s) {
		JSONtokens myObjItem;
		int counter = 0;
		int index;
		size_t found = s.find("{");

		if (found != std::string::npos)
		{
			index = convert(found);										//type_t to int
			found++;													//iterate so that we can move past this token
			myObjects.push_back(myObjItem= { jstring, index });
			//counter++;
			while ( (found = s.find("{", found)) != std::string::npos)	//find inner {'s
			{
				index = convert(found);
				found++;
				//cout << "found inner object at:" << found << endl;
				counter++;
				myObjects.push_back(myObjItem = { jstring, index });
			}
			found = index;
			//cout << "end of {'s" << endl << "\tsearching for matching closing tokens" << endl;
			
			while ((found = s.find("}", found)) != std::string::npos)
			{
				index = convert(found);
				found++;
				myObjects[counter].end = index;
				myObjects[counter].size = myObjects[counter].end - myObjects[counter].start;
				//cout << "ending inner object at:" << found << endl;
				counter--;
				//cout <<  "count= " <<counter << endl;
			}
			//cant find more }'s, check if the outer object complete
			
			//cout << "end object parsing, object tokens generated" << endl;
		}
		cout << "end parseObject function" << endl;
		//printMyObjects();
		return;
	}


	void printMyTokenPairs() {
		cout << "<" << endl;
		for (int i = 0; i < myTokenPairs.size(); i++) {
			int start = myTokenPairs[i].start;
			int end = myTokenPairs[i].end;
			cout << myTokenPairs[i].type << " , " << myTokenPairs[i].start << " , " << myTokenPairs[i].end << " , " << myTokenPairs[i].size;
			cout << " = ";
			for (int j = start; j <= end; j++)
				cout << JSONtext[j];
			cout << endl << endl;

		}
		cout << ">" << endl;
		//print the text version
		cout << endl;
	}

		
	void parseTokenPairs(string s) {
		JSONtokens tokenItem;
		int index;
		int counter = 0;
		size_t found = 0;

		//1. find next key. start of the key is the start of the tokenPair
		//2. find :	just to make sure we have the key and not a random string
		//3. iterate through whitespace.
		//4. locate VALUE through options: {,[, ", -, 0-9, t, f, n     -> and then check for commas
			//4.1 if {, search through vector of tokenObjects with the start at the index we are at
				//4.1.1  tokenPair.end    =    tokenObject.end 
			//4.2 if [, search through vector of tokenArrays with '' 
				//see 4.1.1
			//4.3 if -, keep searching for digits
			//4.4 if t/f, their sizes are fixed.
			//4.5 if null, size is fixed.

		//5 see step 1. 

		//find first set of quotes, end is marked after a colon, not necessarily 4th quote
			//can have 2 or 4 quotes in final string being indexed.

		int keyIndex=0;			//guessIndex do not increment until pair found
		string temp;
		while ((found = s.find("\"", found)) != std::string::npos)
		{
			index = convert(found);
			keyIndex = index;
			found++;
			
			//find matching quote
				//might be logic error here
			if ((found = s.find("\"", found)) != std::string::npos)
			{
				//find the closing quote
				//assume things are good, found the closing quote
				index = convert(found);
				found++;
			}
			//find trailing colon, skipping any whitespace
			while (s[++index] == ' ')
			{
				found++;
			}
			if ( s[index] == ':') {
				myTokenPairs.push_back(tokenItem = { jstring, keyIndex });
			}
			else
			{
				continue;
			}
			//iterate through whitespace
			while (s[++index] == ' ')
			{
				//index = convert(found);
				//found++;
				//index++;
			}
			//search for cases: 

			//strings
			if (s[index] == '"') {
				for (int i = 0; i < myStrings.size(); i++)
				{
					if (myStrings[i].start == index)
					{
						myTokenPairs[counter].end = myStrings[i].end;
						myTokenPairs[counter].size = myTokenPairs[counter].end - myTokenPairs[counter].start;
						counter++;
						break;			//?
					}
				}
			}
			//primitives
			for (int i = 0; i < myPrimitives.size(); i++)
			{
				if (myPrimitives[i].start == index)
				{
					myTokenPairs[counter].end = myPrimitives[i].end;
					myTokenPairs[counter].size = myTokenPairs[counter].end - myTokenPairs[counter].start;
					counter++;
					break;			//?
				}
			}

			//objects
			if (s[index] == '{') {
				//loop through the vector of objects, matching their starting positions? 
				for (int i = 0; i < myObjects.size(); i++)
				{
					if (myObjects[i].start == index)
					{
						//the end of my objects is a }
						myTokenPairs[counter].end = myObjects[i].end;
						myTokenPairs[counter].size = myTokenPairs[counter].end - myTokenPairs[counter].start;
						//pair complete!, goto next pair matching
						counter++;
						//break;
					}
				}
				continue;
			}

			//arrays
			if (s[index] == '[')
			{
				//loop through the vector of objects, matching their starting positions? 
				for (int i = 0; i < myObjects.size(); i++)
				{
					if (myArrays[i].start == index)
					{
						//the end of my objects is a }
						myTokenPairs[counter].end = myArrays[i].end;
						myTokenPairs[counter].size = myTokenPairs[counter].end - myTokenPairs[counter].start;
						counter++;
						break;
					}
				}
			}
		}
	}//end parsetokens
};
	void myObjectTester() {
		string fileoutput;
		string finaloutput;
		ifstream myfile;
		stringstream sstr;
		try {
			
			//myfile.open("test.json");
			myfile.open("test2.json");
			string fullcontents, linecontents;
			while (!myfile.eof())
			{
				getline(myfile, linecontents);
				fullcontents += linecontents;
			}
			cout << fullcontents << endl;

			myJSONParser myParseObject(fullcontents);
			//myParseObject.printMyArrays();
			//myParseObject.printMyObjects();
			//myParseObject.printMyPrimitives();
			//myParseObject.printMyStrings();
			//myParseObject.printMyTokenPairs();
		}
		catch (exception e){cout << "an exception occurred:" << endl << e.what() << endl;}
		catch (...) { cout << "unknown exception" << endl; }
		cout << "object test complete." << endl;
		return;
	}

	int main() {
		string fileoutput;
		//my main method
		//will call my unit tester
		//which will call the parser function
		//which will take a json object
		//file containing ( strings, numbers, {objects}, [arrays] )
		//finally representing it in another form

		//implementing character encoding conversion not required
		//explain where it's needed
		//explain how it would be implemented

		//http://www.w3schools.com/html/html_charset.asp
		//http://www.w3schools.com/charsets/ref_html_utf8.asp
		/*i was mostly focused on ascii encoding in school, 127 values to represent characters */
		/*utf-8 covers a lot.		from the W3 website:
			-UTF-8 is identical to ASCII for the values from 0 to 127
			-UTF-8 DOES NOT use the values from 128 to 159.  (ansi does)
			-UTF-8 is identical to both ANSI and 8859-1 for the values from 160 to 255
			-UTF-8 continues from the value 256 with more than 10 000 different characters.
		*/
		/*So, character encoding is needed at any character value in the range of 128 to 159.
			"string data is represented as utf-8 internally":
			we could print line by line using UTF-8 until , checking the line if any character displays this case.
			To display it, we would have to find the value from the latter 10 000 characters.
			*/

		myObjectTester();


		//string data in UTF-8
		//parser handle Unicode data;
		//myUnitTester();


		return 0;
	}
